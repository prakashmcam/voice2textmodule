# voice2text Module

## Description

TODO: Enter your module description here

## Accessing the voice2text Module

To access this module from JavaScript, you would do the following:

	var voice2text = require("learappcelerator.voice2text");

The voice2text variable is a reference to the Module object.	


### voice2text.setLanguage_model(String languageCode);

set the language model value. This code must be in 
voice2text.setLanguage_model("en_GB");.

### voice2text.listenVoice(callbackFunction)

voice2text.listenVoice(function (result){
		Ti.API.info("getResultText ="+result.resultString);
	});

## Usage

Refer app.js in example directory

## Author

Prakash.M
Software Engineer. 

## License

Open source 2013 @ copy rights.
