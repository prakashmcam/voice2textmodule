// This is a test harness for your module
// You should do something interesting in this harness 
// to test out the module and to provide instructions 
// to users on how to use it by example.

var voice2text = require('learappcelerator.voice2text');
	Ti.API.info("module is => " + voice2text);
	voice2text.setLanguage_model("en_GB");
// open a single window
var win = Ti.UI.createWindow({
	backgroundColor:'white',
	layout:"vertical"
});
var label = Ti.UI.createLabel({
	text: " click and  speech",
	top:50,
	font:{ fontSize:30},
	width:Ti.UI.SIZE,
	color:"#F00"
});

var clickToSpeechBtn=Ti.UI.createButton({
	title:"click"
});
win.add(clickToSpeechBtn);
win.add(label);
win.open();


var successCallback=function (result){
	Ti.API.info("getResultText ="+result.resultString);
	label.text = result.resultString;
}

clickToSpeechBtn.addEventListener('click',function(e){
	
	voice2text.listenVoice(function (result){
		Ti.API.info("getResultText ="+result.resultString);
		label.text = result.resultString;
	});
});


var activity = Ti.Android.currentActivity;
activity.addEventListener('resume', function(e) {
    Ti.API.info('The ' + e.type + ' event happened');
});